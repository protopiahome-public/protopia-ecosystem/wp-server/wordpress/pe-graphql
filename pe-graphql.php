<?php


/**
 * Plugin Name: PE GraphQL
 * Plugin URI: https://www.kb.protopia-home.ru
 * Description: The very first plugin that I have ever created.
 * Version: 1.0
 * Author: delisoft@gmail.com
 * Author URI: https://www.kb.protopia-home.ru
 */

require_once(__DIR__ . "/vendor/autoload.php");
use GraphQL\GraphQL;
use GraphQL\Type\Schema;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\SchemaConfig;
use GraphQL\Utils\Utils;
use GraphQL\Error\Debug;
use GraphQL\Utils\SchemaPrinter;

add_action('parse_request', ["PEGraphql", 'graphql_url_handler']);

class PEGraphql {

    static $types = [
        "Query" => [],
        "Mutation" => [],
        "object_types" => [],
        "input_types" => [],
    ];

    static function object_type($name) {
        if (!isset(self::$types["object_types"][$name]))
        {
            self::$types["object_types"][$name] = new ObjectType([
                "name" => "pegraphql_temp_{$name}",
                "fields" => ['id' => Type::string()],
            ]);
        }
        return self::$types["object_types"][$name];
    }

    static function input_type($name) {
        if (!isset(self::$types["input_types"][$name]))
        {
            self::$types["input_types"][$name] = new InputObjectType([
                "name" => "pegraphql_temp_{$name}",
                "fields" => ['id' => Type::string()],
            ]);
        }
        return self::$types["input_types"][$name];
    }

    static function add_query($name, $query) {
        self::$types["Query"][$name] = $query;
    }

    static function add_mutation($name, $mutation) {
        self::$types["Mutation"][$name] = $mutation;
    }
    
    static function add_object_type($data) {
        if (!count($data["fields"]))
        {
            wp_die($data["name"]);
        }
        if (isset(self::$types["object_types"][$data["name"]]))
        {
            self::$types["object_types"][$data["name"]]->__construct($data);
            return self::$types["object_types"][$data["name"]];
        }
		$data = apply_filters("pe_graphql_object_type_" . $data["name"], $data);
        self::$types["object_types"][$data["name"]] = new ObjectType($data);
    }

    static function add_input_type($data) {
        if (!count($data["fields"]))
        {
            wp_die($data["name"]);
        }
        foreach ($data["fields"] as &$field)
        {
            if (is_array($field))
            {
                unset($field["resolve"]);
            }
        }
        if (isset(self::$types["input_types"][$data["name"]]))
        {
            self::$types["input_types"][$data["name"]]->__construct($data);
            return self::$types["input_types"][$data["name"]];
        }
		$data = apply_filters("pe_graphql_input_type_" . $data["name"], $data);
        self::$types["input_types"][$data["name"]] = new InputObjectType($data);
    }

    static function graphql_url_handler() {
        if($_SERVER["REQUEST_URI"] == '/graphql') {
            header("Access-Control-Allow-Origin: *");
            header("Access-Control-Allow-Headers: *");

            $headers = getallheaders();
            if (isset($headers["Authorization"])){
                $headers["Authorization"] = preg_replace("/^Bearer /", "", $headers["Authorization"]);
                $jws = new \Gamegos\JWS\JWS();
                $jwt_token = $jws->verify($headers["Authorization"], GRAPHQL_JWT_AUTH_SECRET_KEY);
                if ($jwt_token) 
				{
					//$user_id = apply_filters( "pegq_get_tocken", $jwt_token["payload"]["sub"], $jwt_token );
					//if($user_id)
					//	wp_set_current_user( $user_id );
						wp_set_current_user( $jwt_token["payload"]["sub"]);
                }
            }

            do_action("pe_graphql_make_schema");
            
            $schema_array = [];
            $config = SchemaConfig::create();
            if (self::$types["Query"]) {
                $queryType = new ObjectType([
                    'name' => 'Query',
                    'fields' => self::$types["Query"]
                ]);
                $config->setQuery($queryType);
            }
            if (self::$types["Mutation"]) {
                $mutationType = new ObjectType([
                    'name' => 'Mutation',
                    'fields' => self::$types["Mutation"]
                ]);
                $config->setMutation($mutationType);
            }

            $config->setTypes(array_merge(self::$types["object_types"], self::$types["input_types"]));

            try {
                $schema = new Schema($config);
//            echo SchemaPrinter::doPrint($schema);
                
                $rawInput = file_get_contents('php://input');
                if (isset($_POST["operations"])) {
                    $rawInput = stripslashes_deep($_POST["operations"]);
                }
                $input = json_decode($rawInput, true);
                $query = $input['query'];
                $variableValues = isset($input['variables']) ? $input['variables'] : null;
                //wp_die([array_keys(self::$types["object_types"]), array_keys(self::$types["input_types"]), array_keys(self::$types["Query"]), array_keys(self::$types["Mutation"])]);
                $rootValue = ['prefix' => 'You said: '];
                $result = GraphQL::executeQuery($schema, $query, $rootValue, null, $variableValues);

                $debug = \GraphQL\Error\Debug::INCLUDE_DEBUG_MESSAGE || \GraphQL\Error\Debug::RETHROW_UNSAFE_EXCEPTIONS;
                $schema->assertValid();
                $output = $result->toArray($debug);
            } catch (\Exception $e) {
                $output = [
                    'errors' => [
                        [
                            'message' => $e->getMessage()
                        ]
                    ]
                ];
            }
            header('Content-Type: application/json');
            echo json_encode($output);
            
            exit();
        }
    }
}
require_once(__DIR__ . "/auth.php");
require_once(__DIR__ . "/settings.php");